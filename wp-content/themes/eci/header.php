<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ECI
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/images/fav/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/images/fav/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/fav/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/images/fav/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/fav/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/images/fav/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/images/fav/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/images/fav/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/fav/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/images/fav/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/fav/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/images/fav/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/fav/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/fav/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/fav/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>


</head>

<body <?php body_class(); ?> id="home" data-spy="scroll" data-target="#mainMenuContainer">

    	    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127596248-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127596248-1');
</script>

	<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="mobile-menu">
	    <div class="mobile-menu-header">
	        <div class="pull-left text"><?php echo __('Menu','eci'); ?></div>
	        <div class="pull-right close" id="mobile-menu-close">&#10006;</div>
	        <div class="clearfix"></div>
	    </div>
	    <?php /* ?><ul id="mobile-menu" class="sm sm-clean main-menu mobile-menu nav">
	        <li><a class="scroll-menu-item" href="#home">HOME</a></li>
	        <li><a class="scroll-menu-item" href="#about-eci">ABOUT ECI</a></li>
	        <li><a class="scroll-menu-item" href="#products">PRODUCTS</a></li>
	        <li><a class="scroll-menu-item" href="#contact-us">CONTACT US</a></li>
	    </ul><?php */ ?>
	    <?php
	    	wp_nav_menu(array(
	    			'menu' => 'Main Menu',
	    			'menu_class' => 'sm sm-clean main-menu mobile-menu nav',
	    			'menu_id' => 'mobile-menu',
	    			'container' => '',
	    			'theme_location' => 'Primary'
	    		));
	    ?>
	    <!-- <h3>Menu</h3>
	    <a href="#">Celery seakale</a>
	    <a href="#">Dulse daikon</a>
	    <a href="#">Zucchini garlic</a>
	    <a href="#">Catsear azuki bean</a>
	    <a href="#">Dandelion bunya</a>
	    <a href="#">Rutabaga</a> -->
	</nav>

	<?php $login_link = get_option('login_link'); ?>
	<?php $show_login_link = get_option('show_login_link'); ?>
	<?php $show_language_switcher = get_option('show_language_switcher'); ?>

	<header class="header">
	    <div class="container the-container header-container">
	        <div class="header-top">
	            <div class="menu-top-elements">

	            	<?php if( '1' == $show_login_link ) { ?>
	                <span class="login-wrap pull-left"><a href="<?php if( FALSE != $login_link ) { echo $login_link; } else { echo 'javascript:void(0);'; } ?>"><i class="fas fa-user"></i> &nbsp;&nbsp;Login</a></span>
	                <?php } ?>

	                <?php if( '1' == $show_language_switcher ) { ?>
	                <span class="language-wrap pull-right">
	                	<!-- <a href="#">عربي</a> -->
	                	<?php echo do_action('wpml_add_language_selector'); ?>
	                </span>
	                <?php } ?>

	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="left-logo-container">
	            <div class="logo-wrap">
	                <a href="<?php echo site_url(); ?>">
	                	<?php $left_logo = get_option('left_logo'); ?>
	                	<?php if( FALSE != $left_logo ) { ?>
	                	<img src="<?php echo $left_logo; ?>" alt="Etihad Credit Insurance">
	                	<?php } ?>
	                </a>
	            </div>
	        </div>
	        <div class="middle-block-container">
	            <div class="middle-block-wrap">
	                <div class="menu-top">
	                    <div class="menu-top-elements">
	                    	<?php if( '1' == $show_login_link ) { ?>
	                        <span class="login-wrap"><a href="<?php if( FALSE != $login_link ) { echo $login_link; } else { echo 'javascript:void(0);'; } ?>"><i class="fas fa-user"></i> &nbsp;&nbsp;Login</a></span>
	                        <?php } ?>

	                        <?php if( '1' == $show_language_switcher ) { ?>
	                        <span class="language-wrap">
	                        	<!-- <a href="#">عربي</a> -->
	                        	<?php echo do_action('wpml_add_language_selector'); ?>
	                        	<?php // my_custom_switcher(); ?>
	                        </span>
	                        <?php } ?>

	                        <div class="clearfix"></div>
	                    </div>
	                </div>
	                <div class="menu-wrap" id="mainMenuContainer">
	                    <?php /* ?><ul id="main-menu" class="sm sm-clean main-menu desktop-menu nav">
	                        <li><a class="scroll-menu-item" href="#home">HOME</a></li>
	                        <li><a class="scroll-menu-item" href="#about-eci">ABOUT ECI</a></li>
	                        <li><a class="scroll-menu-item" href="#products">PRODUCTS</a></li>
	                        <li><a class="scroll-menu-item" href="#contact-us">CONTACT US</a></li>
	                        <li><a href="#">Item 2</a>
	                            <ul>
	                                <li><a href="#">Item 2-1</a></li>
	                                <li><a href="#">Item 2-2</a></li>
	                                <li><a href="#">Item 2-3</a></li>
	                            </ul>
	                        </li>
	                    </ul><?php */ ?>
	                    <?php
	                    	wp_nav_menu(array(
	                    			'menu' => 'Main Menu',
	                    			'menu_class' => 'sm sm-clean main-menu desktop-menu nav',
	                    			'menu_id' => 'main-menu',
	                    			'container' => '',
	                    			'theme_location' => 'Primary'
	                    		));
	                    ?>
	                    <div class="hamburger" id="hamburger-icon">
	                        <span class="line"></span>
	                        <span class="line"></span>
	                        <span class="line"></span>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="right-logo-container">
	            <div class="logo-wrap">
	            	<?php $right_logo = get_option('right_logo'); ?>
	            	<?php if( FALSE != $right_logo ) { ?>
	            	<a href="javascript:void(0);"><img src="<?php echo $right_logo; ?>" alt="Government of United Arab Emirates"></a>
	            	<?php } ?>
	            </div>
	        </div>
	        <?php /* ?><div class="logo-container">
	            <div class="logo-wrap">
	                <a href="/tony-enterprises"><img src="images/logo.png" alt="Digital Monkey Logo"></a>
	            </div>
	        </div>
	        <div class="pull-right main-menu-container" id="myScrollspy">
	            <ul id="main-menu" class="sm sm-clean main-menu nav">
	                <li><a class="scroll-menu-item" href="/tony-enterprises">HOME</a></li>
	                <li><a class="scroll-menu-item" href="about.php">ABOUT US</a></li>
	                <li>
	                    <a class="scroll-menu-item" href="products.php">PRODUCTS</a>
	                    <ul>
	                        <li><a href="#">Item 2-1</a></li>
	                        <li><a href="#">Item 2-2</a></li>
	                        <li><a href="#">Item 2-3</a></li>
	                    </ul>
	                </li>
	                <li><a class="scroll-menu-item" href="brands.php">BRANDS</a></li>
	                <li><a class="scroll-menu-item" href="clients.php">CLIENTS</a></li>
	                <li><a class="scroll-menu-item" href="awards.php">AWARDS</a></li>
	                <li><a class="scroll-menu-item" href="gallery.php">GALLERY</a></li>
	                <li><a class="scroll-menu-item" href="contact.php">CONTACT US</a></li>
	                <!-- <li><a href="#">Item 2</a>
	                    <ul>
	                        <li><a href="#">Item 2-1</a></li>
	                        <li><a href="#">Item 2-2</a></li>
	                        <li><a href="#">Item 2-3</a></li>
	                    </ul>
	                </li> -->
	            </ul>
	        </div>

	        <div class="clearfix"></div><?php */ ?>
	    </div>
	</header>
	<div class="scroll-menu" id="scroll-menu">
	    <div class="container the-container">
	        <div class="menu-block pull-left">
	            <?php /* ?><ul id="fixed-main-menu" class="sm sm-clean main-menu desktop-menu nav">
	                <li><a class="scroll-menu-item" href="#home">HOME</a></li>
	                <li><a class="scroll-menu-item" href="#about-eci">ABOUT ECI</a></li>
	                <li><a class="scroll-menu-item" href="#products">PRODUCTS</a></li>
	                <li><a class="scroll-menu-item" href="#contact-us">CONTACT US</a></li>
	                <li><a href="#">Item 2</a>
	                    <ul>
	                        <li><a href="#">Item 2-1</a></li>
	                        <li><a href="#">Item 2-2</a></li>
	                        <li><a href="#">Item 2-3</a></li>
	                    </ul>
	                </li>
	            </ul><?php */ ?>
	            <?php
	            	wp_nav_menu(array(
	            			'menu' => 'Main Menu',
	            			'menu_class' => 'sm sm-clean main-menu desktop-menu nav',
	            			'menu_id' => 'fixed-main-menu',
	            			'container' => '',
	            			'theme_location' => 'Primary'
	            		));
	            ?>
	            <div class="hamburger" id="hamburger-icon2">
	                <span class="line"></span>
	                <span class="line"></span>
	                <span class="line"></span>
	            </div>
	        </div>
	        <div class="login-block pull-right">
	        	<?php if( '1' == $show_login_link ) { ?>
	            <span class="login-wrap"><a href="<?php if( FALSE != $login_link ) { echo $login_link; } else { echo 'javascript:void(0);'; } ?>"><i class="fas fa-user"></i> &nbsp;Login</a></span>
	            <?php } ?>

	            <?php if( '1' == $show_language_switcher ) { ?>
	            <span class="language-wrap">
	            	<!-- <a href="#">عربي</a> -->
	            	<?php echo do_action('wpml_add_language_selector'); ?>
	            </span>
	            <?php } ?>

	        </div>
	        <div class="clearfix"></div>
	    </div>
	</div>
