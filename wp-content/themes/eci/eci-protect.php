<?php
/**
 * The template for displaying all pages
 * Template Name: ECI Template
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ECI
 */

get_header();




  if(isset($_POST['submit'])){
    // Open temp file pointer To Write CSV File
    $filepath = get_template_directory().'/file.csv';
    if (!$fp = fopen($filepath, 'w')) return FALSE;

    $error = false;
    $file_error = false;
    $captcha_error = false;
    if(isset($_POST['gtoken'])&&$_POST['gtoken']!=''){
        //open connection
      $ch = curl_init();
      //set the url, number of POST vars, POST data
      $data = array(
        'secret' => '6LeuC7AUAAAAAG_heLQFpsN3yi7L8CBFxYYZaOV9',
        'response' => $_POST['gtoken']
      );
      curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

      $response = curl_exec($ch);


      curl_close($ch);
      $response = json_decode($response);
      if($response->success){
        if($response->score<0.5){
          $error = true;
          $captcha_error = true;
        }
      }
      else{
        $error = true;
        $captcha_error = true;
      }
    }
    else{
      $error = true;
      $captcha_error = true;
    }


    if(isset($_POST['package'])&&$_POST['package']!=''){
      fputcsv($fp, array('Package', $_POST['package']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_name'])&&$_POST['company_name']!=''){
      fputcsv($fp, array('Company Name', $_POST['company_name']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_address'])&&$_POST['company_address']!=''){
      fputcsv($fp, array('Company Address', $_POST['company_address']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_city'])&&$_POST['company_city']!=''){
      fputcsv($fp, array('Company City', $_POST['company_city']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_contact_position'])&&$_POST['company_contact_position']!=''){
      fputcsv($fp, array('Company Contact Position', $_POST['company_contact_position']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_contact_email'])&&$_POST['company_contact_email']!=''){
      fputcsv($fp, array('Company Contact Email', $_POST['company_contact_email']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_registration_no'])&&$_POST['company_registration_no']!=''){
      fputcsv($fp, array('Company Registration No', $_POST['company_registration_no']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_contact_name'])&&$_POST['company_contact_name']!=''){
      fputcsv($fp, array('Company Contact Name', $_POST['company_contact_name']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_contact_tel'])&&$_POST['company_contact_tel']!=''){
      fputcsv($fp, array('Company Contact Tel', $_POST['company_contact_tel_code'].' '.$_POST['company_contact_tel']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['nature_of_business'])&&$_POST['nature_of_business']!=''){
      fputcsv($fp, array('Company Nature of Business', $_POST['nature_of_business']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_establishment_date'])&&$_POST['company_establishment_date']!=''){
      fputcsv($fp, array('Company date of establishment', $_POST['company_establishment_date']));
    }
    else{
      $error = true;
    }
    if(isset($_POST['company_years'])&&$_POST['company_years']!=''){
      fputcsv($fp, array('Company Years since establishment', $_POST['company_years']));
    }
    else{
      $error = true;
    }
    //Count Number of buyers
    if(isset($_POST['buyer_company_name'])&&$_POST['buyer_company_name']!=''){
      $number_of_buyers = sizeof($_POST['buyer_company_name']);
    }
    else{
      $error = true;
    }


    if($number_of_buyers<1){
      $error = true;
    }
    else{
      for($i=0;$i<$number_of_buyers;$i++){
        $j = $i+1;
        if(isset($_POST['buyer_company_name'][$i])&&$_POST['buyer_company_name'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.' Company Name', $_POST['buyer_company_name'][$i]));
        }
        else{
          $error = true;
        }
        if(isset($_POST['buyer_country'][$i])&&$_POST['buyer_country'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.' Country', $_POST['buyer_country'][$i]));
        }
        else{
          $error = true;
        }
        if(isset($_POST['buyer_payment_terms'][$i])&&$_POST['buyer_payment_terms'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.' Term of payment', $_POST['buyer_payment_terms'][$i]));
        }
        else{
          $error = true;
        }
        if(isset($_POST['buyer_contact_name'][$i])&&$_POST['buyer_contact_name'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.'  Contact Name', $_POST['buyer_contact_name'][$i]));
        }
        else{
          $error = true;
        }
        if(isset($_POST['buyer_zip'][$i])&&$_POST['buyer_zip'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.' ZIP', $_POST['buyer_zip'][$i]));
        }
        else{
          $error = true;
        }
        if(isset($_POST['buyer_address'][$i])&&$_POST['buyer_address'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.' Address', $_POST['buyer_address'][$i]));
        }
        else{
          $error = true;
        }
        if(isset($_POST['buyer_credit_limit'][$i])&&$_POST['buyer_credit_limit'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.' Credit Limit', $_POST['buyer_credit_limit'][$i]));
        }
        else{
          $error = true;
        }
        if(isset($_POST['buyer_registration_number'][$i])&&$_POST['buyer_registration_number'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.' Registration Number', $_POST['buyer_registration_number'][$i]));
        }
        else{
          $error = true;
        }
        if(isset($_POST['buyer_vat_no'][$i])&&$_POST['buyer_vat_no'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.' VAT No', $_POST['buyer_vat_no'][$i]));
        }
        else{
          $error = true;
        }
        if(isset($_POST['buyer_city'][$i])&&$_POST['buyer_city'][$i]!=''){
          fputcsv($fp, array('Buyer-'.$j.' City', $_POST['buyer_city'][$i]));
        }
        else{
          $error = true;
        }
      }
    }
    // Place stream pointer at beginning
    rewind($fp);
    fclose($fp);

    $attachments = array($filepath);
    $upload = uploadTempFile('trade_license');
    if($upload){
      $attachments[] = $upload;
    }
    else{
      $error = true;
      $file_error = true;
    }



    $upload = uploadTempFile('eid');
    if($upload){
      $attachments[] = $upload;
    }
    else{
      $error = true;
      $file_error = true;
    }

    $upload = uploadTempFile('passport');
    if($upload){
      $attachments[] = $upload;
    }
    else{
      $error = true;
      $file_error = true;
    }

    $upload = uploadTempFile('pot');
    if($upload){
      $attachments[] = $upload;
    }
    else{
      $error = true;
      $file_error = true;
    }


    if(!$error){
      $to = "firas.sleibi@utsme.com";
      $to = "smeprotectapp@eci.gov.ae";
      $ref = hexdec(uniqid());
      $subject = "New SME Protect Application - $ref";
      $data = array(
        'title' => 'New Application #'.$ref,
        'message' => '<p>Dear Member, You have new SME Application.</p>'
      );
      $message = sme_man_template($data);
      $headers = array('Content-Type: text/html; charset=UTF-8');
      wp_mail($to,$subject,$message,$headers,$attachments);
      $to = $_POST['company_contact_email'];
      $subject = "SME Protect Application Confirmation- $ref";
      $data = array(
        'title' => "SME Protect Application Confirmation- $ref",
        'message' => '<p>Dear '.$_POST['company_contact_name'].', This is to confirm our receipt of your application no '.$ref.'.</p>'
      );
      $message = sme_man_template($data);
      wp_mail($to,$subject,$message,$headers);
      foreach($attachments as $file){
        unlink($file);
      }
    }

  }
?>
<?php if(isset($_POST['submit'])&&!$error){?>
  <div class="entry-content eci-protect-page">
    <div class="success-block">
      <div class="tick"><i class="flaticon-checked"></i></div>
      <h1><?php   _e('Success','eci'); ?></h1>
      <p><?php   _e('Thank you for registering','eci'); ?></p>
      <p><?php   _e('One of our team members will get back to you.','eci'); ?></p>
      <a href="/"><?php   _e('OK','eci'); ?></a>
    </div>
  </div>
<?php }else{?>
  <div class="entry-content eci-protect-page">
    <h1><?php   _e('SME Protect','eci'); ?></h1>
    <ul class="description">
      <li><?php _e('<b>SME Protect</b> is a cost-effective and easy to access and manage solution to move on from the limiting and traditional Letters of Credit or cash payments terms, towards the most updated sales on open credit terms.'); ?></li>
      <li><?php _e('<b>SME Protect</b> is aimed at easing UAE businesses to broaden their understanding of trade credit solutions and to accelerate their export business in a safe way. By providing guarantees to receivables, SMEs can now provide credit to clients without financial loss. To know more about SME Protect, kindly write to us on '); ?>
      <a href="mailto:Smeprotect@eci.gov.ae">Smeprotect@eci.gov.ae</a></li>
    </ul>
    <?php if($captcha_error){ ?>
      <div class="error-form"><?php   _e('Google Captcha Validation Failed, Kindly resubmit.','eci'); ?></div>
    <?php }
          else if($file_error){ ?>
      <div class="error-form"><?php   _e('File upload error, Please make sure file extension is allowed and file size does not exceed 2MB.','eci'); ?></div>
    <?php }
          else if($error){ ?>
      <div class="error-form"><?php   _e('Something wrong happened,Please try again.','eci'); ?></div>
    <?php }?>
    <form method="POST"  enctype="multipart/form-data">
    <div class="white-box-wrapper">

      <h1><?php   _e('Select the required premium from the premium matrix table below','eci'); ?></h1>
      <div class="premium-table-wrapper">
        <table>
          <thead>
            <tr>
              <?php if(wp_is_mobile()){ ?>
                <th></th>
              <?php } ?>
              <th><?php   _e('Premium (AED)','eci'); ?></th>
              <th><?php   _e('Limits','eci'); ?></th>
              <th><?php   _e('Max Limit (AED)','eci'); ?></th>
              <th><?php   _e('Max Liability Amount (AED)','eci'); ?></th>
            </tr>
          </thead>
          <tbody>

            <tr>
              <?php if(wp_is_mobile()){ ?>
                <td><input name="package" id="package1" value="package1" type="radio" /></td>
              <?php } ?>
              <td>10,750</td>
              <td>5</td>
              <td>15,000</td>
              <td>30,000</td>
            </tr>
            <tr>
              <?php if(wp_is_mobile()){ ?>
                <td><input name="package" id="package2" value="package2" type="radio" /></td>
              <?php } ?>
              <td>20,750</td>
              <td>5</td>
              <td>25,000</td>
              <td>50,000</td>
            </tr>
            <tr>
              <?php if(wp_is_mobile()){ ?>
                <td><input name="package" id="package3" value="package3" type="radio" /></td>
              <?php } ?>
              <td>26,000</td>
              <td>10</td>
              <td>35,000</td>
              <td>135,000</td>
            </tr>
            <tr>
              <?php if(wp_is_mobile()){ ?>
                <td><input name="package" id="package4" value="package4" type="radio" /></td>
              <?php } ?>
              <td>35,000</td>
              <td>10</td>
              <td>45,000</td>
              <td>185,000</td>
            </tr>

          </tbody>
        </table>
        <?php if(!wp_is_mobile()){ ?>

        <div class="multi-select">
          <div class="wrapper">
            <input name="package" id="package1" value="package1" type="radio" />
            <label for="package1"><i class="flaticon-checked"></i><span class="select"><?php _e('Select');?></span><span class="selected"><?php _e('Selected');?></span></label>

          </div>
          <div class="wrapper">
            <input name="package" id="package2" value="package2" type="radio" />
            <label for="package2"><i class="flaticon-checked"></i><span class="select"><?php _e('Select');?></span><span class="selected"><?php _e('Selected');?></span></label>

          </div>
          <div class="wrapper">
            <input name="package" id="package3" value="package3" type="radio" />
            <label for="package3"><i class="flaticon-checked"></i><span class="select"><?php _e('Select');?></span><span class="selected"><?php _e('Selected');?></span></label>

          </div>
          <div class="wrapper">
            <input name="package" id="package4" value="package4" type="radio" />
            <label for="package4"><i class="flaticon-checked"></i><span class="select"><?php _e('Select');?></span><span class="selected"><?php _e('Selected');?></span></label>

          </div>




        </div>
        <?php } ?>
      </div>
    </div>
    <div class="white-box-wrapper">
      <h1><?php _e('Fill in the required information','eci') ?></h1>
      <div class="form-section">
        <h1><?php _e('Company Details','eci') ?></h1>
        <div class="form-row">
          <div class="form-col">
            <input type="text" name="company_name" placeholder="<?php _e('Registered company name','eci') ?>" required/>
            <input type="text" name="company_address" placeholder="<?php _e('Registered address','eci') ?>" required/>
            <input type="text" name="company_contact_position" placeholder="<?php _e('Contact\'s position','eci') ?>" required/>
            <input type="email" name="company_contact_email" placeholder="<?php _e('Contact\'s Email ID','eci') ?>" required/>
          </div>
          <div class="form-col">
            <input type="number" min="1"  step="1" name="company_registration_no" placeholder="<?php _e('Company registration number','eci') ?>" required/>
            <select class="company_city" name="company_city" required>
              <option></option>
              <option value="Abu Dhabi"><?php  _e('Abu Dhabi','eci') ?></option>
              <option value="Ajman"><?php  _e('Ajman','eci') ?></option>
              <option value="Dubai"><?php  _e('Dubai','eci') ?></option>
              <option value="Fujairah"><?php  _e('Fujairah','eci') ?></option>
              <option value="Ras Al Khaimah"><?php  _e('Ras Al Khaimah','eci') ?></option>
              <option value="Sharjah"><?php  _e('Sharjah','eci') ?></option>
              <option value="Umm Al Quwain"><?php  _e('Umm Al Quwain','eci') ?></option>
            </select>
            <input type="text" name="company_contact_name" placeholder="<?php _e('Contact\'s name','eci') ?>" required/>
            <?php
            global $wpdb;
            $countries = $wpdb->get_results( "SELECT * FROM tbl_country", OBJECT );
            ?>
            <div class="phone-select-wrapper">
              <select name="company_contact_tel_code" class="phonecodeselect">
                <?php foreach($countries as $country){
                  if($country->ISO2 == "AE"){
                  ?>
                    <option selected value="+<?php echo $country->{'Phone Code'}?>" ><?php echo strtolower($country->ISO2)?></option>
                  <?php }else{?>
                    <option disabled value="+<?php echo $country->{'Phone Code'}?>"><?php echo strtolower($country->ISO2)?></option>
                    <?php
                  }
                } ?>
              </select>
              <input type="number" min="0" name="company_contact_tel" placeholder="<?php _e('Contact\'s number','eci') ?>" required/>
            </div>
          </div>
        </div>
      </div>
      <div class="form-section">
        <h1><?php _e('Nature of Business','eci') ?></h1>
        <div class="form-row">
          <div class="form-col">
            <?php
            global $wpdb;
            $results = $wpdb->get_results("select * from {$wpdb->prefix}business_nature");
            ?>
            <select class="nature_of_business" name="nature_of_business" required>
              <option></option>
              <?php foreach($results as $nature){ ?>
              <option value="<?php echo $nature->name; ?>"><?php echo $nature->name; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-col">
            <input autocomplete="off" type="text"  name="company_establishment_date" placeholder="<?php _e('Date of Establishment','eci') ?>"/>
            <input type="hidden" min="0" name="company_years" placeholder="<?php _e('Number of years since Establishment','eci') ?>"/>
            <div class="number-of-years hidden"><?php _e('Number of years since Establishment','eci') ?>: <span>10</span></div>
          </div>
        </div>
      </div>
      <div class="form-section buyers-section">

        <button class="add_buyers"><i class="flaticon-add"></i><?php _e('Add Buyers','eci') ?></button>
      </div>



    </div>
    <div class="white-box-wrapper">
      <h1><?php _e('Upload the required below document','eci') ?></h1>
      <div class="upload-row">
        <div>
          <button type="button" onclick="$('input[name=trade_license]').click()" for="trade_license"><i class="flaticon-attach"></i><?php _e('Trade License','eci'); ?></button>
          <input accept=".jpg,.jpeg,.png,.pdf" type="file" name="trade_license" />
        </div>
        <div>
          <button type="button" onclick="$('input[name=eid]').click()"><i class="flaticon-attach"></i><?php _e('Emirates ID of Signatory','eci'); ?></button>
          <input accept=".jpg,.jpeg,.png,.pdf"  type="file" name="eid" />
        </div>
        <div>
          <button type="button" onclick="$('input[name=passport]').click()"><i class="flaticon-attach"></i><?php _e('Passport Copy of Signatory','eci'); ?></button>
          <input accept=".jpg,.jpeg,.png,.pdf"  type="file" name="passport" />
        </div>

        <div>
          <button type="button" onclick="$('input[name=pot]').click()"><i class="flaticon-attach"></i><?php _e('Power of Attorney Letter','eci'); ?></button>
          <input accept=".jpg,.jpeg,.png,.pdf"  type="file" name="pot" />

        </div>

      </div>
      <small><?php _e('* Allowed files (PDF, JPG, JPEG, PNG)') ?></small>
      <small><?php _e('** Max file size 2MB') ?></small>
    </div>
    <div class="terms-box">
      <div>
        <input type="checkbox" id="declaration"/>
        <label for="declaration"><div class="checkbox"><i class="flaticon-checked"></i></div><?php _e('I declare that the statements made in this form are true and this form does not withhold any material facts. I understand that non-disclosure or misrepresentation of a material fact will entitle ECI to avoid this insurance.','ecit'); ?></label>
      </div>
      <div>
        <input type="checkbox" id="agreement"/>
        <label for="agreement"><div class="checkbox"><i class="flaticon-checked"></i></div><?php _e('I understand that the signing of this proposal does not bind me to complete the insurance but agree that, should a contract of insurance be concluded, this proposal and the statements made herein shall form the basis of the contract and be incorporated therein.','ecit'); ?></label>
      </div>
    </div>
    <input type="hidden" name="gtoken"/>
    <input name="submit" type="submit" value="<?php _e('Submit','eci');?>" style="display:none"/>
    <button onclick="submitForm()" type="button" class="submit-button"><?php _e('Submit','eci');?></button>
    </form>
  </div>
<?php } ?>

  <div class="buyer-template">
    <div class="form-section buyers">
      <h1><?php _e('Buyers Details','eci') ?></h1>
      <div class="form-row">
        <div class="form-col">
          <input type="text" name="buyer_company_name[]" placeholder="<?php _e('Registered company name','eci') ?>" required/>
          <?php
          global $wpdb;
          $results = $wpdb->get_results("select * from {$wpdb->prefix}countries");
          ?>
          <select class="buyer_country" name="buyer_country[]" required>
            <option></option>
            <?php foreach($results as $country){ ?>
            <option value="<?php echo $country->name; ?>"><?php echo $country->name; ?></option>
            <?php } ?>
          </select>
          <input type="text" name="buyer_payment_terms[]" placeholder="<?php _e('Terms of payment','eci') ?>" required/>
          <input type="text" name="buyer_contact_name[]" placeholder="<?php _e('Contact\'s Name','eci') ?>" required/>
          <input type="number"  min="1"  step="1" name="buyer_zip[]" placeholder="<?php _e('ZIP Code','eci') ?>" required/>
        </div>
        <div class="form-col">
          <input type="text" name="buyer_address[]" placeholder="<?php _e('Address','eci') ?>" required/>
          <input type="number" class="credit_limit" min="0" name="buyer_credit_limit[]" placeholder="<?php _e('Credit limit requried (AED)','eci') ?>" required/>
          <input type="number" min="1"  step="1" name="buyer_registration_number[]" placeholder="<?php _e('Company registration number','eci') ?>" required/>
          <input type="number"  min="1"  step="1" name="buyer_vat_no[]" placeholder="<?php _e('VAT number','eci') ?>" required/>
          <input type="text" name="buyer_city[]" placeholder="<?php _e('City','eci') ?>" required/>
        </div>
      </div>

      <a href="#" class="remove-buyer"><?php _e('Remove Buyer','eci') ?></a>
    </div>
  </div>
  <script>
  var buyers_i = 2;
  $("input[name=package]").on('change',function(){
    var package_id = $("input[name='package']:checked").val();
    if(package_id == "package1"){
      $(".credit_limit").attr("max",15000);
    }
    if(package_id == "package2"){
      $(".credit_limit").attr("max",25000);
    }
    if(package_id == "package3"){
      $(".credit_limit").attr("max",35000);
    }
    if(package_id == "package4"){
      $(".credit_limit").attr("max",45000);
    }
  });
  function formatPhone(data){
    if (!data.id) { return data.text; }
    		var $currency = $('<img src="<?php echo get_template_directory_uri()."/images/flags/" ?>' + data.element.innerHTML + '.png"/> <span>' + data.element.value + '</span>');
    return $currency;
  }
  jQuery(document).ready(function() {
      $(".add_buyers").before($(".buyer-template").html());
      jQuery('.phonecodeselect').select2({
        placeholder: "<?php _e('Select Country','eci');?>",
        templateResult: formatPhone,
        templateSelection: formatPhone,
      });
      jQuery('.nature_of_business').select2({
        placeholder: "<?php _e('Nature of Business','eci');?>",
        allowClear: true
      });
      jQuery('form .buyer_country').select2({
        placeholder: "<?php _e('Country','eci');?>",
        allowClear: true
      });
      jQuery('form .company_city').select2({
        placeholder: "<?php _e('City','eci');?>",
        allowClear: true
      });
      //Set max date
      $( "input[name=company_establishment_date]" ).datepicker({
        maxDate: 0,
        changeMonth: true,
        changeYear: true,
      });
      $( "input[name=company_establishment_date]" ).change(function(){
        var years = new Date(new Date() - new Date($( "input[name=company_establishment_date]" ).val())).getFullYear() - 1970;
        $(".number-of-years").removeClass('hidden');
        $("input[name=company_years]").val(years);
        $(".number-of-years span").html(years);
      });
  });
  $(document).on("change","input[type=file]",function(e){
    var fileName = e.target.files[0].name;
    $(this).next(".attachment-block").remove();
    $(this).after('<div class="attachment-block"><i class="flaticon-attachment"></i> '+fileName+' <i data-remove="'+$(this).attr("name")+'" class="flaticon-error"></i></div>');
  });
  $(document).on("click",".attachment-block .flaticon-error",function(e){
    var input_name= $(this).attr("data-remove");
    $("input[name="+input_name+"]").val("");
    $(this).closest(".attachment-block").remove();

  });
  $(document).on("click",".add_buyers",function(e){
    if($("input[name='package']:checked").val()==undefined){
      alertify.alert('ECI', '<?php _e('Please select package first.','eci') ?>', function(){ });
      return;
    }
    var package_id = $("input[name='package']:checked").val();
    var limit;
    if(package_id == 'package1' || package_id == 'package2'){
      limit = 6;
    }
    if(package_id == 'package3' || package_id == 'package4'){
      limit = 11;
    }
    if(buyers_i >= limit){
      alertify.alert('ECI', '<?php _e('You have reached your selected buyers limit.','eci') ?>', function(){ });
      return;
    }
    $(".buyer-template>div>h1").html("<?php _e('Buyers Details','eci') ?> " +" "+ buyers_i);
    $("form .buyer_country").select2("destroy");
    $(".add_buyers").before($(".buyer-template").html());
    el = jQuery('form .buyer_country')[buyers_i - 1];

    $.each($("form .buyer_country"),function(i,obj){
      $(this).select2({
        placeholder: "<?php _e('Country','eci');?>",
        allowClear: true
      });
    });

    buyers_i++;
    return false;

  });
  $(document).on("click",".remove-buyer",function(e){
    $(this).closest(".form-section").remove();
    buyers_i = $("form .buyers").length + 1;
    $.each($("form .buyers"),function(i,obj){
      buyer_new_i = i + 1;
      $(obj).find(">h1").html("<?php _e('Buyers Details','eci') ?> " +" "+ buyer_new_i);
    });
    return false;

  });
  $(document).on("submit","form",function(){
    $(".error-field").remove();
    var error = false;
    if($("input[name=trade_license]").val()==""||$("input[name=eid]").val()==""||$("input[name=passport]").val()==""||$("input[name=pot]").val()==""){
      $(".upload-row").prepend("<div class='error-field'><?php _e('Please upload all fields.','eci') ?></div>");
      error =true;
    }

    if($("input[name='package']:checked").val()==undefined){
      $(".premium-table-wrapper").prepend("<div class='error-field'><?php _e('Please select package.','eci') ?></div>");
      error =true;
    }
    if($("#agreement:checked").val()==undefined||$("#declaration:checked").val()==undefined){
      $(".terms-box").prepend("<div class='error-field'><?php _e('Please accept all agreements','eci') ?></div>");
      error =true;
    }
    if(error){
      document.querySelector('.error-field').scrollIntoView({
      behavior: 'smooth'
    });
    }

    return !error;


  });
  function submitForm(){
    grecaptcha.execute('6LeuC7AUAAAAAJ1dBHqj7FzAmPsa4eMf8wf3Iefd', {action: 'homepage'}).then(function(token) {
      $("input[name=gtoken]").val(token);
       $('input[type="submit"]').click();

    });
  }
  </script>
  <script src="https://www.google.com/recaptcha/api.js?render=6LeuC7AUAAAAAJ1dBHqj7FzAmPsa4eMf8wf3Iefd"></script>
  <script>
  grecaptcha.ready(function() {

  });
  </script>
<?php
get_footer();

function uploadTempFile($field_name){

  $target_dir = $_SERVER['DOCUMENT_ROOT']. '/temp/';
  $image_name = $_FILES[$field_name]['name'];
  $target_file = $target_dir . basename($_FILES[$field_name]['name']);
  $uploadOk = 1;
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));


  // Check file size

  if ($_FILES["YOUR_IMAGE"]["size"] > 200000) {
      //echo "Sorry, your file is too large.";
      $uploadOk = 0;
  }

  // Allow certain file formats
  if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg") {

      $uploadOk = 1;
      // Check if image file is a actual image or fake image
      $check = getimagesize($_FILES[$field_name]["tmp_name"]);
      if($check !== false) {
          $uploadOk = 1;
      } else {
        //echo "Not image";
          $uploadOk = 0;
      }

  }
  else if($imageFileType == "pdf"){
    $uploadOk = 1;
  }
  else{
    //echo "file type error ".$imageFileType;
    $uploadOk = 0;
  }
  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
      return false;
      // if everything is ok, try to upload file
  } else {
      if (move_uploaded_file($_FILES[$field_name]["tmp_name"], $target_file)) {
          return $target_file;
      } else {
          return false;
      }
  }

}
