<?php
/**
 * ECI functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ECI
 */

if ( ! function_exists( 'eci_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function eci_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ECI, use a find and replace
		 * to change 'eci' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'eci', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'eci' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'eci_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'eci_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function eci_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'eci_content_width', 640 );
}
add_action( 'after_setup_theme', 'eci_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function eci_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'eci' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'eci' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'eci_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function eci_scripts() {

	// Styles
	wp_enqueue_style( 'main-styles', get_template_directory_uri() . '/css/main-styles.css' );
	/*wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/css/animate.css' );
	wp_enqueue_style( 'lightgallery-css', get_template_directory_uri() . '/css/lightgallery.css' );*/
	wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri() . '/css/owl.carousel.min.css' );
	wp_enqueue_style( 'owl-theme-css', get_template_directory_uri() . '/css/owl.theme.default.min.css' );
	wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/assets/slick/slick.css' );
	wp_enqueue_style( 'slick-theme-css', get_template_directory_uri() . '/assets/slick/slick-theme.css' );
	wp_enqueue_style( 'sm-core-css', get_template_directory_uri() . '/css/sm-core-css.css' );
	wp_enqueue_style( 'sm-clean-css', get_template_directory_uri() . '/css/sm-clean.css' );
	wp_enqueue_style( 'fontawesome5-css', get_template_directory_uri() . '/assets/fontawesome5/web-fonts-with-css/css/fontawesome-all.css' );
	wp_enqueue_style( 'component-css', get_template_directory_uri() . '/css/component.css' );
	wp_enqueue_style( 'OverlayScrollbars-css', get_template_directory_uri() . '/css/OverlayScrollbars.css' );
	wp_enqueue_style( 'video-slick-css', get_template_directory_uri() . '/css/video-slick.css' );
	wp_enqueue_style( 'style-css', get_template_directory_uri() . '/stylesheets/style.css' );
	// wp_enqueue_style( 'digital-monkey-style', get_stylesheet_uri() );

	// Scripts
	wp_enqueue_script( 'jquery-12', get_template_directory_uri() . '/js/jquery.min.js', array(), '' );
	wp_enqueue_script( 'main-scripts', get_template_directory_uri() . '/js/main-scripts.js', array(), '', true );
	/* wp_enqueue_script( 'lightgallery-scripts', get_template_directory_uri() . '/js/lightgallery.min.js', array(), '', true );*/
	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '', true );
	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/slick/slick.min.js', array(), '', true );
	wp_enqueue_script( 'smartmenu-js', get_template_directory_uri() . '/js/jquery.smartmenus.min.js', array(), '', true );
	wp_enqueue_script( 'smoothscroll-js', get_template_directory_uri() . '/js/SmoothScroll.js', array(), '', true );
	wp_enqueue_script( 'classie-js', get_template_directory_uri() . '/js/classie.js', array(), '', true );
	wp_enqueue_script( 'modernizr-custom-js', get_template_directory_uri() . '/js/modernizr.custom.js', array(), '', true );
	wp_enqueue_script( 'OverlayScrollbars-js', get_template_directory_uri() . '/js/OverlayScrollbars.js', array(), '', true );
	wp_enqueue_script( 'OverlayScrollbars-js', get_template_directory_uri() . '/js/OverlayScrollbars.js', array(), '', true );
	wp_enqueue_script( 'jquery-tubular-js', get_template_directory_uri() . '/js/jquery.tubular.1.0.js', array(), '', true );
	wp_enqueue_script( 'jquery-datepicker-js', get_template_directory_uri() . '/jquery-date-picker/jquery-ui.min.js', array(), '', true );
	wp_enqueue_script( 'alertify-js', get_template_directory_uri() . '/alertifyjs/alertify.min.js', array(), '', true );
	// wp_enqueue_script( 'video-slick-js', get_template_directory_uri() . '/js/video-slick.js', array(), '', true );
	/* wp_enqueue_script( 'jquery-nicescroll-js', get_template_directory_uri() . '/js/jquery.nicescroll.js', array(), '', true );
	wp_enqueue_script( 'isotope-js', get_template_directory_uri() . '/js/isotope.min.js', array(), '', true );
	wp_enqueue_script( 'isotope-functions-js', get_template_directory_uri() . '/js/isotope.functions.js', array(), '', true );
	wp_enqueue_script( 'waypoints-js', get_template_directory_uri() . '/js/jquery.waypoints.min.js', array(), '', true );
	wp_enqueue_script( 'imagesloaded-js', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array(), '', true ); */
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array(), '', true );
	wp_enqueue_script( 'select-js', get_template_directory_uri() . '/select2/js/select2.min.js', array(), '' );
	wp_enqueue_style( 'alertify-css', get_template_directory_uri() . '/alertifyjs/css/alertify.min.css' );
	wp_enqueue_style( 'select-css', get_template_directory_uri() . '/select2/css/select2.min.css' );
	wp_enqueue_style( 'alertify-theme-css', get_template_directory_uri() . '/alertifyjs/css/themes/default.min.css' );
	wp_enqueue_style( 'jquery-datepicker-css', get_template_directory_uri() . '/jquery-date-picker/jquery-ui.min.css' );
	wp_enqueue_style( 'secure-font-css', get_template_directory_uri() . '/secure-font/flaticon.css' );
	wp_enqueue_style( 'new-style-css', get_template_directory_uri() . '/style.min.css' ,null,'1.7');


	/*wp_enqueue_style( 'eci-style', get_stylesheet_uri() );

	wp_enqueue_script( 'eci-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'eci-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}*/
}
add_action( 'wp_enqueue_scripts', 'eci_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



// Theme Settings

function theme_settings_page()
{
    ?>
    	<style>
			.theme-settings-form input {
				width: 250px;
			}
			.theme-settings-form input[type="submit"],
			.theme-settings-form input[type="checkbox"] {
				width: auto;
			}
    	</style>
	    <div class="wrap">
	    <h1>Site Common Settings</h1>
	    <form class="theme-settings-form" method="post" action="options.php" enctype="multipart/form-data">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");
	            submit_button();
	        ?>
	    </form>
		</div>
	<?php
}

function add_theme_menu_item()
{
	add_menu_page("Site Common Settings", "Site Common Settings", "manage_options", "site-common-settings", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");


function display_twitter_element()
{
	?>
    	<input type="text" name="twitter_url" id="twitter_url" value="<?php echo get_option('twitter_url'); ?>" />
    <?php
}

function display_facebook_element()
{
	?>
    	<input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" />
    <?php
}

function left_logo_display()
{
	?>
        <input type="file" name="left_logo" />
        <i>Preferred Image Size: 199 x 41</i>
        <br/>

        <?php if( FALSE != get_option('left_logo') ) { ?>
        <img src="<?php echo get_option('left_logo'); ?>" alt="Left Logo" width="150" height="50">
        <?php } ?>
   <?php
}
function handle_left_logo_upload()
{

	if(!empty($_FILES["left_logo"]["tmp_name"]))
	{

		$urls = wp_handle_upload($_FILES["left_logo"], array('test_form' => FALSE));

	    $temp = $urls["url"];
	    return $temp;

	} else {
		$option = get_option('left_logo');
		return $option;
	}


}

function right_logo_display()
{
	?>
        <input type="file" name="right_logo" />
        <i>Preferred Image Size: 246 x 55</i>
        <br/>

        <?php if( FALSE != get_option('right_logo') ) { ?>
        <img src="<?php echo get_option('right_logo'); ?>" alt="Right Logo" width="150" height="50">
        <?php } ?>
   <?php
}

function handle_right_logo_upload()
{

	if(!empty($_FILES["right_logo"]["tmp_name"]))
	{

		$urls = wp_handle_upload($_FILES["right_logo"], array('test_form' => FALSE));
	    $temp = $urls["url"];
	    return $temp;

	} else {
		$option = get_option('right_logo');
		return $option;
	}
}

function display_login_link()
{
	?>
    	<input type="text" name="login_link" id="login_link" value="<?php echo get_option('login_link'); ?>" />
    <?php
}

function display_login_link_element()
{
	?>
		<input type="checkbox" name="show_login_link" value="1" <?php checked(1, get_option('show_login_link'), true); ?> />
	<?php
}

function display_language_switcher_element()
{
	?>
		<input type="checkbox" name="show_language_switcher" value="1" <?php checked(1, get_option('show_language_switcher'), true); ?> />
	<?php
}



function display_theme_panel_fields()
{
	add_settings_section("section", "All Settings", null, "theme-options");

	add_settings_field("twitter_url", "Twitter Profile Url", "display_twitter_element", "theme-options", "section");
    add_settings_field("facebook_url", "Facebook Profile Url", "display_facebook_element", "theme-options", "section");
    add_settings_field("left_logo", "Left Logo", "left_logo_display", "theme-options", "section");
    add_settings_field("right_logo", "Right Logo", "right_logo_display", "theme-options", "section");
    add_settings_field("login_link", "Login Link URL", "display_login_link", "theme-options", "section");
    add_settings_field("show_login_link", "Show Login Link", "display_login_link_element", "theme-options", "section");
    add_settings_field("show_language_switcher", "Show Language Switcher", "display_language_switcher_element", "theme-options", "section");

    register_setting("section", "twitter_url");
    register_setting("section", "facebook_url");
    register_setting("section", "left_logo", "handle_left_logo_upload");
    register_setting("section", "right_logo", "handle_right_logo_upload");
    register_setting("section", "login_link");
    register_setting("section", "show_login_link");
    register_setting("section", "show_language_switcher");
}

add_action("admin_init", "display_theme_panel_fields");



// Language Switcher Order

/*function my_custom_switcher(){
    $languages = icl_get_languages('skip_missing=1&orderby=code');
    if(!empty($languages)){ ?>
        <div id="footer_language_list">
            <ul>

                <?php
                foreach($languages as $l){

                    if( !$l['active'] ){ ?>
                        <li>
                            <a href="<?php echo $l['url']; ?>">
                                <span><?php echo $l['translated_name'] ?></span>
                            <a>
                        </li><?php
                    }

                } ?>
            </ul>
        </div><?php
    }
}*/


// Disabling wordpress update notifications

add_action('after_setup_theme','remove_core_updates');

function remove_core_updates()

{

if(! current_user_can('update_core')){return;}

add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);

add_filter('pre_option_update_core','__return_null');

add_filter('pre_site_transient_update_core','__return_null');

}





// Disables automatic plugin updates

remove_action('load-update-core.php','wp_update_plugins');

add_filter('pre_site_transient_update_plugins','__return_null');

//Include Email Templates

foreach (glob(plugin_dir_path( __FILE__ )."email_templates/*.php") as $filename)
{
    include_once($filename);
}
