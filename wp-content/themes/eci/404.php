<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ECI
 */

get_header();
?>

	<style>
		.notfound-block {
			padding: 18%;
			text-align: center;
		}
		.notfound-block a {
		    color: #1a1a1a;
		}
		.notfound-block a:hover {
		     color: #ad833a;
		}
		h1 {
			font-size: 50px;
			color: #ad833a;
			margin-top: 0px;
			margin-bottom: 25px;
		}
		.back-to-home-btn {
			display: block;
			color: #fff;
			font-size: 22px;
			margin-top: 10px;
		}
		@media(max-width: 767px) {
			.notfound-block {
				padding: 35% 10% 10% 15%;
				text-align: center;
			}
			.back-to-home-btn {
				font-size: 18px;
				margin-top: 10px;
			}
		}
	</style>
	<section class="notfound-block">
				<h1>Oops! Page not found</h1>
		<a href="<?php echo site_url(); ?>" class="back-to-home-btn" >Back to Home</a>
	</section>


<?php
get_footer();
