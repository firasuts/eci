<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ECI
 */

?>

<footer class="footer block-padding-top block-padding-bottom">
    <div class="container the-container">
      <div class="footer-menu">
        <ul>
          <li><a href="/privacy-notice">Privacy Notice</a></li>
          <li><a href="/terms-of-use">Terms of Use</a></li>
        </ul>
      </div>
        <div class="content">
            <p><?php echo __('Copyright','eci'); ?> <?php echo date('Y'); ?>. <?php echo __('ETIHAD CREDIT INSURANCE','eci'); ?></p>
        </div>
    </div>
</footer>


<?php wp_footer(); ?>

<?php $current_lang = ICL_LANGUAGE_CODE; ?>
<?php
  if( $current_lang == 'ar') {
    $rtl_status = 'true';
    $map_lattitude = types_render_field( 'map-lattitude', array( 'id' => 44 ) );
    $map_longitude = types_render_field( 'map-longitude', array( 'id' => 44 ) );
  } else {
    $rtl_status = 'false';
    $map_lattitude = types_render_field( 'map-lattitude', array( 'id' => 5 ) );
    $map_longitude = types_render_field( 'map-longitude', array( 'id' => 5 ) );
  }
?>

<form action="https://maps.google.com/maps" method="get" target="_blank" name="get_dir_form">
<input type="hidden" name="saddr" value="Current Location">
<input type="hidden" name="daddr" value="<?php echo $map_lattitude; ?>,<?php echo $map_longitude; ?>">
</form>

<?php if( is_front_page() ) { ?>

<?php // $map_lattitude = types_render_field( 'map-lattitude', array( 'id' => 5 ) ); ?>
<?php // $map_longitude = types_render_field( 'map-longitude', array( 'id' => 5 ) ); ?>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjDEidwkBY3Tody-v4jOowcGAstdJ7fsI&libraries=places"></script>

<script type="text/javascript">
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init1);

    function init1() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 15,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng("<?php echo $map_lattitude; ?>", "<?php echo $map_longitude; ?>"), // Oman



            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.

             styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#dbdbdb"},{"visibility":"on"}]}],
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
                draggable: true
        };

        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng("<?php echo $map_lattitude; ?>", "<?php echo $map_longitude; ?>"),
            map: map,
            title: 'Mazoon',
            icon: '<?php echo get_template_directory_uri(); ?>/images/location-map-icon.png'
        });

        /*marker.addListener('click', function() {
            console.log('inside');
            $('form[name=get_dir_form]').submit();
        });*/
    }
</script>

<script>

    $(document).ready(function(){

        var rtl_status = <?php echo $rtl_status; ?>;
        $('.home-slk-slider').slick({
          // centerMode: false,
          // centerPadding: '150px',
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: true,
          dots: false,
          autoplay: false,
          autoplaySpeed: 6000,
          speed: 1500,
          draggable: true,
          rtl: rtl_status,
          // appendDots: '#customDots',
          asNavFor: '.home-slk-slider-captions'
        });
        $('.home-slk-slider-captions').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          asNavFor: '.home-slk-slider',
          arrows: true,
          fade: true,
          // centerMode: false,
          focusOnSelect: true,
          autoplay: false,
          autoplaySpeed: 6000,
          speed: 1500,
          draggable: false,
          rtl: rtl_status
        });

        // $('.video-item').tubular({videoId: 'e4Is32W-ppk'});

        $('#media-center-carousel').owlCarousel({
            loop:false,
            margin:30,
            nav:false,
            dots:true,
            mouseDrag: false,
            rtl: rtl_status,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        })
    });
</script>


<script>
    var rtl_status = <?php echo $rtl_status; ?>;
    var slideWrapper = $(".main-slider"),
      iframes = slideWrapper.find(".embed-player"),
      lazyImages = slideWrapper.find(".slide-image"),
      lazyCounter = 0;

    // POST commands to YouTube or Vimeo API
    function postMessageToPlayer(player, command) {
      if (player == null || command == null) return;
      player.contentWindow.postMessage(JSON.stringify(command), "*");
    }

    // When the slide is changing
    function playPauseVideo(slick, control) {
      var currentSlide, slideType, startTime, player, video;

      currentSlide = slick.find(".slick-current");
      slideType = currentSlide.attr("class").split(" ")[1];
      player = currentSlide.find("iframe").get(0);
      startTime = currentSlide.data("video-start");

      if (slideType === "vimeo") {
        switch (control) {
          case "play":
            if (
              startTime != null &&
              startTime > 0 &&
              !currentSlide.hasClass("started")
            ) {
              currentSlide.addClass("started");
              postMessageToPlayer(player, {
                method: "setCurrentTime",
                value: startTime
              });
            }
            postMessageToPlayer(player, {
              method: "play",
              value: 1
            });
            break;
          case "pause":
            postMessageToPlayer(player, {
              method: "pause",
              value: 1
            });
            break;
        }
      } else if (slideType === "youtube") {
        switch (control) {
          case "play":
            postMessageToPlayer(player, {
              event: "command",
              func: "mute"
            });
            postMessageToPlayer(player, {
              event: "command",
              func: "playVideo"
            });
            break;
          case "pause":
            postMessageToPlayer(player, {
              event: "command",
              func: "pauseVideo"
            });
            break;
        }
      } else if (slideType === "video") {
        video = currentSlide.children("video").get(0);
        if (video != null) {
          if (control === "play") {
            video.play();
          } else {
            video.pause();
          }
        }
      }
    }

    // Resize player
    function resizePlayer(iframes, ratio) {
      if (!iframes[0]) return;
      var win = $(".main-slider"),
        width = win.width(),
        playerWidth,
        height = win.height(),
        playerHeight,
        ratio = ratio || 16 / 9;

      iframes.each(function() {
        var current = $(this);
        if (width / ratio < height) {
          playerWidth = Math.ceil(height * ratio);
          current
            .width(playerWidth)
            .height(height)
            .css({
              left: (width - playerWidth) / 2,
              top: 0
            });
        } else {
          playerHeight = Math.ceil(width / ratio);
          current
            .width(width)
            .height(playerHeight)
            .css({
              left: 0,
              top: (height - playerHeight) / 2
            });
        }
      });
    }

    // DOM Ready
    $(function() {
      // Initialize
      slideWrapper.on("init", function(slick) {
        slick = $(slick.currentTarget);
        setTimeout(function() {
          playPauseVideo(slick, "play");
        }, 1000);
        resizePlayer(iframes, 16 / 9);
      });
      slideWrapper.on("beforeChange", function(event, slick) {
        slick = $(slick.$slider);
        playPauseVideo(slick, "pause");
      });
      slideWrapper.on("afterChange", function(event, slick) {
        slick = $(slick.$slider);
        playPauseVideo(slick, "play");
      });
      slideWrapper.on("lazyLoaded", function(event, slick, image, imageSource) {
        lazyCounter++;
        if (lazyCounter === lazyImages.length) {
          lazyImages.addClass("show");
          // slideWrapper.slick("slickPlay");
        }
      });

      //start the slider
      slideWrapper.slick({
        fade:true,
        autoplay: true,
        autoplaySpeed: 8000,
        lazyLoad: "progressive",
        speed: 600,
        arrows: true,
        dots: false,
        rtl: rtl_status,
        cssEase: "cubic-bezier(0.87, 0.03, 0.41, 0.9)"
        // asNavFor: '.home-slk-slider-captions'
      });

      /*$('.home-slk-slider-captions').slick({
        autoplaySpeed: 4000,
        lazyLoad: "progressive",
        speed: 600,
        arrows: false,
        dots: true,
        cssEase: "cubic-bezier(0.87, 0.03, 0.41, 0.9)",
        asNavFor: '.main-slider'
      });*/

    });

    // Resize event
    $(window).on("resize.slickVideoPlayer", function() {
      resizePlayer(iframes, 16 / 9);
    });

</script>

<?php } ?>

</body>
</html>
