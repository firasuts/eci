<?php 
get_header();
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="page-slider-block">
    <div class="custom-icon slider-loader">
      <img src="<?php echo get_template_directory_uri(); ?>/images/slider-loader.svg" alt="Slider Loader">
    </div>
    <div class="home-slk-slider">
        <section class="main-slider">
          <div class="item image">
            <!-- <span class="loading">Loading...</span> -->
            <figure>
              <div class="slide-image slide-media" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/slider1.jpg');">
                <img data-lazy="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" class="image-entity" />
              </div>
              <figcaption class="caption">
                  <h2>Credit Insurance</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </figcaption>
            </figure>
          </div>
          <!-- <div class="item vimeo" data-video-start="4">
            <iframe class="embed-player slide-media" src="https://player.vimeo.com/video/217885864?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=217885864" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen
              allowfullscreen></iframe>
            <p class="caption">Vimeo</p>
          </div> -->
          <div class="item image">
            <figure>
              <div class="slide-image slide-media" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/slider2.jpg');">
                <img data-lazy="<?php echo get_template_directory_uri(); ?>/images/slider2.jpg" class="image-entity" />
              </div>
              <figcaption class="caption">
                  <h2>Surity Bonds</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </figcaption>
            </figure>
          </div>
          <div class="item youtube">
            <iframe class="embed-player slide-media" width="980" height="520" src="https://www.youtube.com/embed/W9dmeRMAqYw?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1" frameborder="0" allowfullscreen></iframe>
            <figcaption class="caption">
                <h2>Export Financing & Guarantees</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </figcaption>
          </div>
          <!-- <div class="item image">
            <figure>
              <div class="slide-image slide-media" style="background-image:url('https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSlBkWDBsWXJNazQ');">
                <img data-lazy="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSlBkWDBsWXJNazQ" class="image-entity" />
              </div>
              <figcaption class="caption">Static Image</figcaption>
            </figure>
          </div> -->
          <!-- <div class="item video">
            <video class="slide-video slide-media" loop muted preload="metadata" poster="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSXZCakVGZWhOV00">
              <source src="https://player.vimeo.com/external/138504815.sd.mp4?s=8a71ff38f08ec81efe50d35915afd426765a7526&profile_id=112" type="video/mp4" />
            </video>
            <p class="caption">HTML 5 Video</p>
          </div> -->
        </section>
    </div>



    <?php /* ?><div class="home-slk-slider">
        <div class="slider-item">
            <div class="slide-image-wrap">
                <img src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="Slider Image">
            </div>
        </div>
        <div class="slider-item">
            <div class="slide-image-wrap">
                <img src="<?php echo get_template_directory_uri(); ?>/images/slider2.jpg" alt="Slider Image">
            </div>
        </div>

    </div>
    <div class="container the-container caption-container">
        <div class="caption-slider-block">
            <div class="home-slk-slider-captions">
                <div class="caption-item">
                    <h2>Credit Insurance</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <div class="caption-item">
                    <h2>Credit Insurance</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <div class="caption-item">
                    <h2>Credit Insurance</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>
        </div>
    </div><?php */ ?>
    <?php /* ?><div class="owl-carousel owl-theme">
        <div class="item-video"><iframe width="420" height="315" src="https://www.youtube.com/watch?v=G71kaqooAbk">
   </iframe></div>
        <div><img src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="Slider Image"></div>
        <div class="item-video"><iframe width="420" height="315" src="https://www.youtube.com/watch?v=G71kaqooAbk">
   </iframe></div>
        <div class="item-video"><iframe width="420" height="315" src="https://www.youtube.com/watch?v=G71kaqooAbk">
   </iframe></div>
        <div class="item-video"><iframe width="420" height="315" src="https://www.youtube.com/watch?v=G71kaqooAbk">
   </iframe></div>
        
    </div><?php */ ?>
</div>

<div class="page-intro home-page-intro block-padding-top block-padding-bottom" id="about-eci">
    <div class="container the-container">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="content">
                    <h1>ABOUT US</h1>
                    <div class="scrollbox">
                        <div class="scrollbox-wrap">
                            <?php the_content(); ?>
                            <?php /* ?><p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p>
                            <p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p>
                            <p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p>
                            <p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p>
                            <p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p><?php */ ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $show_mission_and_vision = get_field('show_mission_and_vision'); ?>
<?php if( $show_mission_and_vision == TRUE ) { ?>

<?php $mission = get_field('mission'); ?>
<?php $vision = get_field('vision'); ?>
<?php if( ( FALSE != $mission ) AND ( FALSE != $vision ) ) { ?>
<div class="horizontal-tabs-block block-padding-top">
    <div class="container the-container">
        <div class="tablinks-wrapper">
            <ul class="nav nav-pills">
                <li class="active"><a data-toggle="pill" href="#our-mission">Our Mission</a></li>
                <li><a data-toggle="pill" href="#our-vision">Our Vision</a></li>
            </ul>
        </div>
        <div class="tabcontents-wrapper">
            <div class="tab-content">
               <div id="our-mission" class="tab-pane fade in active">
                <div class="scrollbox">
                    <div class="scrollbox-wrap">
                        <?php echo $mission; ?>
                    </div>
                </div>
               </div>
               <div id="our-vision" class="tab-pane fade">
                <div class="scrollbox">
                    <div class="scrollbox-wrap">
                        <?php echo $vision; ?>
                    </div>
                 </div>
               </div>
             </div>
        </div>
    </div>
</div>
<?php } else  { ?>
<div class="horizontal-tabs-block block-padding-top">
    <div class="container the-container">
        <div class="content text-center white-bg block-padding-top block-padding-bottom">
            <p>No Content Found</p>
        </div>
    </div>
</div>
<?php } ?>

<?php } ?>

<?php $show_products_block = get_field('show_products_block'); ?>
<?php if( FALSE != $show_products_block ) { ?>

<?php 
$products_args = array('post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'ASC'
            );

$products_query = new WP_Query( $products_args );
// var_dump($products_query);
?>

<?php if( $products_query->have_posts() ) { ?>

<div class="vertical-tabs-block block-padding-top block-padding-bottom" id="products">
    <div class="container the-container">
        <div class="heading-wrapper block-padding-bottom text-center">
            <h2>Our Products</h2>
        </div>
        <div class="vertical-tab-blocks">
            <div class="left-block">
                <div class="tablinks-wrapper">
                    <ul class="nav nav-pills nav-stacked">
                        <?php $v_tabs_links_count = 1; ?>
                        <?php while ( $products_query->have_posts() ) : $products_query->the_post(); ?>
                        <li <?php if( $v_tabs_links_count == 1 ) { echo 'class="active"'; }; ?> >
                            <a data-toggle="pill" href="#<?php echo $post->post_name; ?>" class="display-table">
                                <span class="table-row">
                                    <?php $product_icon = types_render_field( 'product-icon' ); ?>
                                    <?php if( FALSE != $product_icon ) { ?>
                                    <span class="image table-cell">
                                        <span class="content">
                                            <?php echo types_render_field( 'product-icon' ); ?>
                                        </span>
                                    </span>
                                    <?php } else { ?>
                                    <span class="image table-cell">
                                        <span class="content">
                                            <img class="product-icon" src="http://via.placeholder.com/64x64" alt="Credit Insurance">
                                        </span>
                                    </span>
                                    <?php } ?>

                                    <span class="title table-cell">
                                        <span class="content">
                                            <?php the_title(); ?>
                                        </span>
                                    </span>
                                    <span class="clearfix"></span>
                                </span>
                            </a>
                        </li>
                        <?php $v_tabs_links_count++; ?>
                        <?php endwhile; wp_reset_postdata(); ?>

                        <?php /* ?><li class="active"><a data-toggle="pill" href="#credit-insurance"><img class="product-icon" src="<?php echo get_template_directory_uri(); ?>/images/credit-insurance.png" alt="Credit Insurance">Credit Insurance</a></li>
                        <li><a data-toggle="pill" href="#surity-bonds"><img class="product-icon" src="<?php echo get_template_directory_uri(); ?>/images/surity-bonds.png" alt="Security Bonds">Surity Bonds</a></li>
                        <li><a data-toggle="pill" href="#export-financing-gurantees"><img class="product-icon" src="<?php echo get_template_directory_uri(); ?>/images/export-financing-and-guarantees.png" alt="Export Financing & Guarantees">Export Financing & Guarantees</a></li><?php */ ?>

                    </ul>
                </div>
            </div>
            <div class="right-block">
                <div class="tabcontents-wrapper">
                    <div class="tab-content">

                        <?php $v_tabs_content_count = 1; ?>
                        <?php while ( $products_query->have_posts() ) : $products_query->the_post(); ?>
                        <div id="<?php echo $post->post_name; ?>" class="tab-pane fade <?php if( $v_tabs_content_count == 1 ) { echo 'in active'; }; ?>">
                         <h3 class="display-mobile"><?php the_title(); ?></h3>
                         <div class="scrollbox">
                             <div class="scrollbox-wrap">
                                  
                                  <?php the_content(); ?>
                                  
                             </div>
                         </div>
                        </div>
                        <?php $v_tabs_content_count++; ?>
                        <?php endwhile; wp_reset_postdata(); ?>

                       <?php /* ?><div id="credit-insurance" class="tab-pane fade in active">
                        <h3 class="display-mobile">Credit Insurance</h3>
                        <div class="scrollbox">
                            <div class="scrollbox-wrap">
                                 
                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p> 

                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p>
                            </div>
                        </div>
                       </div>
                       <div id="surity-bonds" class="tab-pane fade">
                        <h3 class="display-mobile">Surity Bonds</h3>
                         <div class="scrollbox">
                             <div class="scrollbox-wrap">
                                 
                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p> 

                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p>
                             </div>
                         </div>
                       </div>
                       <div id="export-financing-gurantees" class="tab-pane fade">
                        <h3 class="display-mobile">Export Financing & Guarantees</h3>
                        <div class="scrollbox">
                            <div class="scrollbox-wrap">
                                 
                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p> 

                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p>
                             </div>
                         </div>
                       </div><?php */ ?>

                     </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php } else { ?>
<div class="vertical-tabs-block block-padding-top block-padding-bottom" id="products">
    <div class="container the-container">
        <div class="content text-center white-bg block-padding-top block-padding-bottom">
            <p>No Products Found</p>
        </div>
    </div>
</div>
<?php } ?>

<?php } ?>

<?php $show_contact_information_block = types_render_field( 'show-contact-information-block' ); ?>
<?php if( $show_contact_information_block == "1") { ?>

<div class="contact-block">
    <div id="contact-us"></div>
    <div id="map" class="map"></div>
    <div class="location-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/images/location-icon.png" alt="Location Icon">
    </div>
    <div class="contact-info-block">
        <div class="container the-container">

            <div class="contact-info-block">
                <div class="contact-details">
                    <div class="heading-wrapper">
                        <h2>Contact Us</h2>
                    </div>
                    <?php 
                    $contact_address = types_render_field( 'contact-address', array( 'output' => 'raw', 'separator' => '|' ) );
                    $contact_addresses = ( explode("|", $contact_address) );
                    ?>

                    <?php if( FALSE != $contact_address ) { ?>
                    <div class="address-block">
                        <div class="row">

                            <?php foreach( $contact_addresses as $contact_addresse_item ) { ?>

                            <div class="col-xs-6 address-block-col">
                                <div class="content">
                                    <p><?php echo $contact_addresse_item; ?></p>
                                </div>
                            </div>

                            <?php } ?>

                            <?php /* ?><div class="col-xs-6 address-block-col">
                                <div class="content">
                                    <p>P.O BOX 94200<br/>
                                        Abu Dhabi, UAE</p>
                                </div>
                            </div>
                            <div class="col-xs-6 address-block-col">
                                <div class="content">
                                    <p>P.O BOX 29595<br/>
                                        Dubai, UAE</p>
                                </div>
                            </div><?php */ ?>

                        </div>
                    </div>
                    <?php } ?>

                    <?php 
                    $contact_telephone = types_render_field( 'contact-telephone', array( 'output' => 'raw', 'separator' => ', ' ) );
                    $contact_dir = types_render_field( 'contact-dir', array( 'output' => 'raw', 'separator' => ', ' ) );
                    $contact_email = types_render_field( 'contact-email', array( 'output' => 'raw', 'separator' => ', ' ) );
                    // $contact_telephones = ( explode("|", $contact_telephone) );
                    ?>

                    <div class="contact-media-block">
                        <p>Tel: <?php echo $contact_telephone; ?><br/>
                        Dir: <?php echo $contact_dir; ?><br/>
                        Email: <?php echo $contact_email; ?></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>

<?php endwhile; endif; wp_reset_postdata(); ?>

<?php
get_footer();