<?php 
get_header();
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <?php 
  $page_slider_args = array('post_type' => 'custom-page-slider',
              'post_status' => 'publish',
              'posts_per_page' => -1,
              'orderby' => 'date',
              'order' => 'DESC'
              );

  $page_slider_query = new WP_Query( $page_slider_args );
  // var_dump($page_slider_query);
  ?>

  <?php 

  $video_flag = 0;

  if( $page_slider_query->have_posts() ) {
    while ( $page_slider_query->have_posts() ) : $page_slider_query->the_post();
      $custom_slide_type_check = types_render_field( 'custom-slide-type' );
      if( $custom_slide_type_check == "Video" ) {
        $video_flag++;
      }
    endwhile; wp_reset_postdata();
  }
  // var_dump($video_flag);
  ?>

  <?php if( $video_flag > 0) { ?>

  <?php if( $page_slider_query->have_posts() ) { ?>



  <div class="page-slider-block">
      <div class="custom-icon slider-loader">
        <img src="<?php echo get_template_directory_uri(); ?>/images/slider-loader.svg" alt="Slider Loader">
      </div>
      <div class="home-slk-slider">
          <section class="main-slider">
            
            <?php while ( $page_slider_query->have_posts() ) : $page_slider_query->the_post(); ?>
            <?php $custom_slide_type = types_render_field( 'custom-slide-type' ); ?>
            <?php $custom_show_slide_content = types_render_field( 'custom-show-slide-content' ); ?>
            <?php if( $custom_slide_type == "Image" ) { ?>

            <?php 
            $custom_slide_image = types_render_field( 'custom-slide-image', array( 'url' => true, 'size' => 'slider-image' ) );
            $custom_slide_image_id = attachment_url_to_postid( $custom_slide_image );
            $custom_slide_image_alt = get_post_meta( $custom_slide_image_id, '_wp_attachment_image_alt', true);
            ?>
            <div class="item image">
              <!-- <span class="loading">Loading...</span> -->
              <figure>
                <div class="slide-image slide-media" style="background-image:url('<?php if( FALSE != $custom_slide_image) { echo $custom_slide_image; } else { echo 'http://via.placeholder.com/1141x431'; } ?>');">
                  <img data-lazy="<?php if( FALSE != $custom_slide_image) { echo $custom_slide_image; } else { echo 'http://via.placeholder.com/1141x431'; } ?>" class="image-entity" alt="<?php if( FALSE != $custom_slide_image_alt ) { echo $custom_slide_image_alt; } else { echo 'ECI'; } ?>"/>
                </div>

                <?php if( $custom_show_slide_content == "1" ) { ?>

                <?php 
                $custom_slide_title = types_render_field( 'custom-slide-title' );
                $custom_slide_short_description = types_render_field( 'custom-slide-short-description' );
                ?>
                <figcaption class="caption">
                    <?php if( FALSE != $custom_slide_title ) { ?>
                    <h2><?php echo $custom_slide_title; ?></h2>
                    <?php } ?>
                    
                    <?php if( FALSE != $custom_slide_short_description ) { ?>
                    <p><?php echo $custom_slide_short_description; ?></p>
                    <?php } ?>
                </figcaption>

                <?php } ?>

              </figure>
            </div>
            <?php } else if( $custom_slide_type == "Video" ) { ?>

            <?php 
            $slide_video_url = types_render_field( 'slide-video-url', array( 'output' => 'raw' ) );
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $slide_video_url, $match);
            $youtube_id = $match[1];
            ?>
            
            <div class="item youtube">
              <iframe class="embed-player slide-media" width="980" height="520" src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1" frameborder="0" allowfullscreen></iframe>

              <?php if( $custom_show_slide_content == "1" ) { ?>

              <?php 
              $custom_slide_title = types_render_field( 'custom-slide-title' );
              $custom_slide_short_description = types_render_field( 'custom-slide-short-description' );
              ?>

              <figcaption class="caption">
                  <?php if( FALSE != $custom_slide_title ) { ?>
                  <h2><?php echo $custom_slide_title; ?></h2>
                  <?php } ?>
                  
                  <?php if( FALSE != $custom_slide_short_description ) { ?>
                  <p><?php echo $custom_slide_short_description; ?></p>
                  <?php } ?>
              </figcaption>
              
              <?php } ?>

            </div>

            <?php } ?>

            <?php endwhile; wp_reset_postdata(); ?>

            <?php /* ?><div class="item image">
              <!-- <span class="loading">Loading...</span> -->
              <figure>
                <div class="slide-image slide-media" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/slider1.jpg');">
                  <img data-lazy="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" class="image-entity" />
                </div>
                <figcaption class="caption">
                    <h2>Credit Insurance</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </figcaption>
              </figure>
            </div>
            <!-- <div class="item vimeo" data-video-start="4">
              <iframe class="embed-player slide-media" src="https://player.vimeo.com/video/217885864?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=217885864" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen
                allowfullscreen></iframe>
              <p class="caption">Vimeo</p>
            </div> -->
            <div class="item image">
              <figure>
                <div class="slide-image slide-media" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/slider2.jpg');">
                  <img data-lazy="<?php echo get_template_directory_uri(); ?>/images/slider2.jpg" class="image-entity" />
                </div>
                <figcaption class="caption">
                    <h2>Surity Bonds</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </figcaption>
              </figure>
            </div>
            <div class="item youtube">
              <iframe class="embed-player slide-media" width="980" height="520" src="https://www.youtube.com/embed/W9dmeRMAqYw?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1" frameborder="0" allowfullscreen></iframe>
              <figcaption class="caption">
                  <h2>Export Financing & Guarantees</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </figcaption>
            </div>
            <!-- <div class="item image">
              <figure>
                <div class="slide-image slide-media" style="background-image:url('https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSlBkWDBsWXJNazQ');">
                  <img data-lazy="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSlBkWDBsWXJNazQ" class="image-entity" />
                </div>
                <figcaption class="caption">Static Image</figcaption>
              </figure>
            </div> -->
            <!-- <div class="item video">
              <video class="slide-video slide-media" loop muted preload="metadata" poster="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSXZCakVGZWhOV00">
                <source src="https://player.vimeo.com/external/138504815.sd.mp4?s=8a71ff38f08ec81efe50d35915afd426765a7526&profile_id=112" type="video/mp4" />
              </video>
              <p class="caption">HTML 5 Video</p>
            </div> -->
            <?php */ ?>

          </section>
      </div>



      <?php /* ?><div class="home-slk-slider">
          <div class="slider-item">
              <div class="slide-image-wrap">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="Slider Image">
              </div>
          </div>
          <div class="slider-item">
              <div class="slide-image-wrap">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/slider2.jpg" alt="Slider Image">
              </div>
          </div>

      </div>
      <div class="container the-container caption-container">
          <div class="caption-slider-block">
              <div class="home-slk-slider-captions">
                  <div class="caption-item">
                      <h2>Credit Insurance</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                  </div>
                  <div class="caption-item">
                      <h2>Credit Insurance</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                  </div>
                  <div class="caption-item">
                      <h2>Credit Insurance</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                  </div>
              </div>
          </div>
      </div><?php */ ?>
      <?php /* ?><div class="owl-carousel owl-theme">
          <div class="item-video"><iframe width="420" height="315" src="https://www.youtube.com/watch?v=G71kaqooAbk">
     </iframe></div>
          <div><img src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="Slider Image"></div>
          <div class="item-video"><iframe width="420" height="315" src="https://www.youtube.com/watch?v=G71kaqooAbk">
     </iframe></div>
          <div class="item-video"><iframe width="420" height="315" src="https://www.youtube.com/watch?v=G71kaqooAbk">
     </iframe></div>
          <div class="item-video"><iframe width="420" height="315" src="https://www.youtube.com/watch?v=G71kaqooAbk">
     </iframe></div>
          
      </div><?php */ ?>
  </div>

  <?php } ?>

  <?php } else { ?>

  <div class="page-slider-block slider-without-video" id="slider-without-video">
      <div class="custom-icon slider-loader">
        <img src="<?php echo get_template_directory_uri(); ?>/images/slider-loader.svg" alt="Slider Loader">
      </div>
      <div class="home-slk-slider">

            <?php while ( $page_slider_query->have_posts() ) : $page_slider_query->the_post(); ?>

            <?php 
            $custom_slide_image = types_render_field( 'custom-slide-image', array( 'url' => true, 'size' => 'slider-image' ) );
            $custom_slide_image_id = attachment_url_to_postid( $custom_slide_image );
            $custom_slide_image_alt = get_post_meta( $custom_slide_image_id, '_wp_attachment_image_alt', true);
            ?>

            <div class="slider-item" style="background-image: url('<?php if( FALSE != $custom_slide_image) { echo $custom_slide_image; } else { echo 'http://via.placeholder.com/1141x431'; } ?>');">
                <div class="slide-image-wrap">
                    <img src="<?php if( FALSE != $custom_slide_image) { echo $custom_slide_image; } else { echo 'http://via.placeholder.com/1141x431'; } ?>" alt="<?php if( FALSE != $custom_slide_image_alt ) { echo $custom_slide_image_alt; } else { echo 'ECI'; } ?>">
                </div>
            </div>

            <?php endwhile; wp_reset_postdata(); ?>

            <?php /* ?><div class="slider-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/slider1.jpg');">
                <div class="slide-image-wrap">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="Slider Image">
                </div>
            </div>
            <div class="slider-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/slider2.jpg');">
                <div class="slide-image-wrap">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/slider2.jpg" alt="Slider Image">
                </div>
            </div>
            <div class="slider-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/slider1.jpg');">
                <div class="slide-image-wrap">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="Slider Image">
                </div>
            </div><?php */ ?>

        </div>
        <div class="container the-container caption-container">
            <div class="caption-slider-block">
                <div class="home-slk-slider-captions">

                    <?php while ( $page_slider_query->have_posts() ) : $page_slider_query->the_post(); ?>
                    
                    <?php $custom_show_slide_content = types_render_field( 'custom-show-slide-content' ); ?>
                    <div class="caption-item">
                        <?php if( $custom_show_slide_content == "1" ) { ?>

                        <?php 
                        $custom_slide_title = types_render_field( 'custom-slide-title' );
                        $custom_slide_short_description = types_render_field( 'custom-slide-short-description' );
                        ?>

                        <?php if( FALSE != $custom_slide_title ) { ?>
                        <h2><?php echo $custom_slide_title; ?></h2>
                        <?php } ?>

                        <?php if( FALSE != $custom_slide_short_description ) { ?>
                        <p><?php echo $custom_slide_short_description; ?></p>
                        <?php } ?>
                        
                        <?php } ?>
                        
                    </div>

                    <?php endwhile; wp_reset_postdata(); ?>

                    <?php /* ?><div class="caption-item">
                        <h2>Credit Insurance</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                    <div class="caption-item">
                        <h2>Credit Insurance2</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                    <div class="caption-item">
                        <h2>Credit Insurance3</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div><?php */ ?>
                    
                </div>
            </div>
        </div>
  </div>

  <?php } ?>

<div class="page-intro home-page-intro block-padding-top block-padding-bottom" id="about-eci">
    <div class="container the-container">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="content">
                    <h1><?php echo __('ABOUT US','eci'); ?></h1>
                    <div class="">
                        <div class="">
                            <?php the_content(); ?>
                            <?php /* ?><p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p>
                            <p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p>
                            <p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p>
                            <p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p>
                            <p>Imagine a world without borders. A place where trade and export was so seamless and risk free, businesses can operate at optimum efficiency. With Etihad Credit Insurance – this vision becomes a reality.</p><?php */ ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $show_mission_and_vision = get_field('show_mission_and_vision'); ?>
<?php if( $show_mission_and_vision == TRUE ) { ?>

<?php $mission = get_field('mission'); ?>
<?php $vision = get_field('vision'); ?>
<?php if( ( FALSE != $mission ) AND ( FALSE != $vision ) ) { ?>
<div class="horizontal-tabs-block block-padding-top" id="mission-vision">
    <div class="container the-container">
    	<div class="heading-wrapper block-padding-bottom text-center">
    	    <h2><?php echo __('Mission & Vision','eci'); ?></h2>
    	</div>
        <?php /* ?><div class="tablinks-wrapper">
            <ul class="nav nav-pills">
                <li class="active"><a data-toggle="pill" href="#our-mission"><?php echo __('Our Mission','eci'); ?></a></li>
                <li><a data-toggle="pill" href="#our-vision"><?php echo __('Our Vision','eci'); ?></a></li>
            </ul>
        </div><?php */ ?>
        <div class="tabcontents-wrapper">
            <?php /* ?><div class="tab-content">
               <div id="our-mission" class="tab-pane fade in active">
                <div class="scrollbox">
                    <div class="scrollbox-wrap">
                        <?php echo $mission; ?>
                    </div>
                </div>
               </div>
               <div id="our-vision" class="tab-pane fade">
                <div class="scrollbox">
                    <div class="scrollbox-wrap">
                        <?php echo $vision; ?>
                    </div>
                 </div>
               </div>
             </div><?php */ ?>
             <div class="text-center">
             	<h3><?php echo __('Our Vision','eci'); ?></h3>
             	<br/>	
             	<?php echo $vision; ?>
             	<h3><?php echo __('Our Mission','eci'); ?></h3>
             	<br/>
             	<?php echo $mission; ?>
             </div>
             
        </div>
    </div>
</div>
<?php } else  { ?>
<div class="horizontal-tabs-block block-padding-top">
    <div class="container the-container">
        <div class="content text-center white-bg block-padding-top block-padding-bottom">
            <p><?php echo __('No Content Found','eci'); ?></p>
        </div>
    </div>
</div>
<?php } ?>

<?php } ?>


<?php $eci_show_board_members = get_field('eci_show_board_members'); ?>
<?php if( FALSE != $eci_show_board_members ) { ?>

<?php $eci_board_members = CFS()->get( 'eci_board_members', $post->ID ); ?>
<?php if( FALSE != $eci_board_members ) { ?>

<div class="board-members-block block-padding-top" id="board-members">
  <div class="container the-container">
      <div class="heading-wrapper block-padding-bottom text-center">
          <h2><?php echo __('Our Board Members','eci'); ?></h2>
      </div>
      <div class="board-members-list-block">
        <div class="board-members-row">

          <?php for($i=0;$i<sizeof($eci_board_members);$i++) { ?>
          <?php 
          $eci_board_member_name = $eci_board_members[$i]['eci_board_member_name'];
          $eci_board_member_designation = $eci_board_members[$i]['eci_board_member_designation'];
          $eci_board_member_image_id = $eci_board_members[$i]['eci_board_member_image'];
          $eci_board_member_image_alt = get_post_meta( $eci_board_member_image_id, '_wp_attachment_image_alt', true );
          $eci_board_member_image_values = wp_get_attachment_image_src( $eci_board_member_image_id, 'slider-image' );
          $eci_board_member_image_url = $eci_board_member_image_values[0];
          $eci_board_member_description = $eci_board_members[$i]['eci_board_member_description'];
          ?>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <?php /* ?><div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php if( FALSE != $eci_board_member_image_url ) { echo $eci_board_member_image_url; } else { echo get_template_directory_uri().'/images/board-member-default.jpg'; } ?>" alt="<?php if( FALSE != $eci_board_member_image_alt ) { echo $eci_board_member_image_alt; } else { echo 'Board Member'; } ?>">
                      </div>
                    </div><?php */ ?>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p><?php echo $eci_board_member_name; ?></p>
                        <p class="designation"><?php echo $eci_board_member_designation; ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php if( FALSE != $eci_board_member_description ){ ?>
              <div class="board-member-body">
                <div class="content">
                  <p><?php echo $eci_board_member_description; ?></p>
                </div>
              </div>
	          <?php } ?>
            </div>
          </div>

          <?php } ?>

          <?php /* ?><div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/board-member.jpg" alt="Board Member">
                      </div>
                    </div>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p>His Highness Sheikh Hamdan bin Rashid Al Maktoum</p>
                        <p>CHAIRMAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="board-member-body">
                <div class="content">
                  <p>The Deputy Ruler of Dubai and the Minister of Finance of the UAE</p>
                </div>
              </div>
            </div>
          </div><?php */ ?>

        </div>
      </div>
  </div>
</div>

<?php } else { ?>

<div class="board-members-block block-padding-top">
  <div class="container the-container">
    <div class="heading-wrapper block-padding-bottom text-center">
        <h2><?php echo __('Our Board Members','eci'); ?></h2>
    </div>
    <div class="content text-center white-bg block-padding-top block-padding-bottom no-content-div">
        <p><?php echo __('No Content Found','eci'); ?></p>
    </div>
  </div>
</div>

<?php } ?>

<?php $eci_management_members = CFS()->get( 'eci_management_members', $post->ID ); ?>
<?php if( FALSE != $eci_management_members ) { ?>

<div class="board-members-block">
  <div class="container the-container block-padding-top border-top-yellow">
      <div class="heading-wrapper block-padding-bottom text-center">
          <h2><?php echo __('Management','eci'); ?></h2>
      </div>
      <div class="board-members-list-block">
        <div class="board-members-row">

          <?php for($i=0;$i<sizeof($eci_management_members);$i++) { ?>
          <?php 
          $eci_management_member_name = $eci_management_members[$i]['eci_management_member_name'];
          $eci_management_member_designation = $eci_management_members[$i]['eci_management_member_designation'];
          $eci_management_member_image_id = $eci_management_members[$i]['eci_management_member_image'];
          $eci_management_member_image_alt = get_post_meta( $eci_management_member_image_id, '_wp_attachment_image_alt', true );
          $eci_management_member_image_values = wp_get_attachment_image_src( $eci_management_member_image_id, 'slider-image' );
          $eci_management_member_image_url = $eci_management_member_image_values[0];
          $eci_management_member_description = $eci_management_members[$i]['eci_management_member_description'];
          ?>

          <div class="board-member-col">
            <div class="board-member-thumb">
              <div class="board-member-head">
                <div class="display-table">
                  <div class="table-row">
                    <?php /* ?><div class="table-cell member-cell left-cell">
                      <div class="content">
                        <img src="<?php if( FALSE != $eci_management_member_image_url ) { echo $eci_management_member_image_url; } else { echo get_template_directory_uri().'/images/board-member-default.jpg'; } ?>" alt="<?php if( FALSE != $eci_management_member_image_alt ) { echo $eci_management_member_image_alt; } else { echo 'Board Member'; } ?>">
                      </div>
                    </div><?php */ ?>
                    <div class="table-cell member-cell right-cell">
                      <div class="content">
                        <p><?php echo $eci_management_member_name; ?></p>
                        <p><?php echo $eci_management_member_designation; ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php if( FALSE != $eci_management_member_description ){ ?>
              <div class="board-member-body">
                <div class="content">
                  <p><?php echo $eci_management_member_description; ?></p>
                </div>
              </div>
	          <?php } ?>
            </div>
          </div>

          <?php } ?>

        </div>
      </div>
  </div>
</div>

<?php } else { ?>

<div class="board-members-block">
  <div class="container the-container block-padding-top">
    <div class="heading-wrapper block-padding-bottom text-center">
        <h2><?php echo __('Management','eci'); ?></h2>
    </div>
    <div class="content text-center white-bg block-padding-top block-padding-bottom no-content-div">
        <p><?php echo __('No Content Found','eci'); ?></p>
    </div>
  </div>
</div>

<?php } ?>

<?php } ?>




<?php $show_products_block = get_field('show_products_block'); ?>
<?php if( FALSE != $show_products_block ) { ?>

<?php 
$products_args = array('post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'ASC'
            );

$products_query = new WP_Query( $products_args );
// var_dump($products_query);
?>

<?php if( $products_query->have_posts() ) { ?>

<div class="vertical-tabs-block block-padding-top" id="products">
    <div class="container the-container">
        <div class="heading-wrapper block-padding-bottom text-center">
            <h2><?php echo __('Our Products','eci'); ?></h2>
        </div>
        <div class="vertical-tab-blocks">
            <div class="left-block">
                <div class="tablinks-wrapper">
                    <ul class="nav nav-pills nav-stacked">
                        <?php $v_tabs_links_count = 1; ?>
                        <?php while ( $products_query->have_posts() ) : $products_query->the_post(); ?>
                        <li <?php if( $v_tabs_links_count == 1 ) { echo 'class="active"'; }; ?> >
                            <a data-toggle="pill" href="#<?php echo $post->post_name; ?>" class="display-table">
                                <span class="table-row">
                                    <?php $product_icon = types_render_field( 'product-icon', array('url' => 'true') ); ?>
                                    <?php if( FALSE != $product_icon ) { ?>
                                    <?php 
                                    $product_icon_id = attachment_url_to_postid( $product_icon );
                                    $product_icon_alt = get_post_meta( $product_icon_id, '_wp_attachment_image_alt', true);
                                    ?>
                                    <span class="image table-cell">
                                        <span class="content">
                                            <img src="<?php echo $product_icon; ?>" alt="<?php if( FALSE != $product_icon_alt ) { echo $product_icon_alt; } else { echo 'Etihad Credit Insurance'; } ?>">
                                            <?php // echo types_render_field( 'product-icon' ); ?>
                                        </span>
                                    </span>
                                    <?php } else { ?>
                                    <span class="image table-cell">
                                        <span class="content">
                                            <img class="product-icon" src="http://via.placeholder.com/64x64" alt="Credit Insurance">
                                        </span>
                                    </span>
                                    <?php } ?>

                                    <span class="title table-cell">
                                        <span class="content">
                                            <?php the_title(); ?>
                                        </span>
                                    </span>
                                    <span class="clearfix"></span>
                                </span>
                            </a>
                        </li>
                        <?php $v_tabs_links_count++; ?>
                        <?php endwhile; wp_reset_postdata(); ?>

                        <?php /* ?><li class="active"><a data-toggle="pill" href="#credit-insurance"><img class="product-icon" src="<?php echo get_template_directory_uri(); ?>/images/credit-insurance.png" alt="Credit Insurance">Credit Insurance</a></li>
                        <li><a data-toggle="pill" href="#surity-bonds"><img class="product-icon" src="<?php echo get_template_directory_uri(); ?>/images/surity-bonds.png" alt="Security Bonds">Surity Bonds</a></li>
                        <li><a data-toggle="pill" href="#export-financing-gurantees"><img class="product-icon" src="<?php echo get_template_directory_uri(); ?>/images/export-financing-and-guarantees.png" alt="Export Financing & Guarantees">Export Financing & Guarantees</a></li><?php */ ?>

                    </ul>
                </div>
            </div>
            <div class="right-block">
                <div class="tabcontents-wrapper">
                    <div class="tab-content">

                        <?php $v_tabs_content_count = 1; ?>
                        <?php while ( $products_query->have_posts() ) : $products_query->the_post(); ?>
                        <div id="<?php echo $post->post_name; ?>" class="tab-pane fade <?php if( $v_tabs_content_count == 1 ) { echo 'in active'; }; ?>">
                         <h3 class="display-mobile"><?php the_title(); ?></h3>
                         <div class="scrollbox">
                             <!-- <div class="scrollbox-wrap"> -->
                                  
                                  <?php the_content(); ?>
                                  
                             <!-- </div> -->
                         </div>
                        </div>
                        <?php $v_tabs_content_count++; ?>
                        <?php endwhile; wp_reset_postdata(); ?>

                       <?php /* ?><div id="credit-insurance" class="tab-pane fade in active">
                        <h3 class="display-mobile">Credit Insurance</h3>
                        <div class="scrollbox">
                            <div class="scrollbox-wrap">
                                 
                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p> 

                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p>
                            </div>
                        </div>
                       </div>
                       <div id="surity-bonds" class="tab-pane fade">
                        <h3 class="display-mobile">Surity Bonds</h3>
                         <div class="scrollbox">
                             <div class="scrollbox-wrap">
                                 
                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p> 

                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p>
                             </div>
                         </div>
                       </div>
                       <div id="export-financing-gurantees" class="tab-pane fade">
                        <h3 class="display-mobile">Export Financing & Guarantees</h3>
                        <div class="scrollbox">
                            <div class="scrollbox-wrap">
                                 
                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p> 

                                 <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p>
                             </div>
                         </div>
                       </div><?php */ ?>

                     </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php } else { ?>
<div class="vertical-tabs-block block-padding-top block-padding-bottom" id="products">
    <div class="container the-container">
        <div class="content text-center white-bg block-padding-top block-padding-bottom">
            <p><?php echo __('No Content Found','eci'); ?></p>
        </div>
    </div>
</div>
<?php } ?>

<?php } ?>







<?php $eci_show_media_center_block = get_field('eci_show_media_center_block'); ?>
<?php if( FALSE != $eci_show_media_center_block ) { ?>

<?php $eci_media_list = CFS()->get( 'eci_media_list', $post->ID ); ?>
<?php if( FALSE != $eci_media_list ) { ?>

<div class="media-center-block block-padding-top">
  <div class="container the-container media-center">
      <div class="heading-wrapper block-padding-bottom text-center">
          <h2><?php echo __('Media Center','eci'); ?></h2>
      </div>


      <div class="media-center-block-body">
        <div class="owl-carousel owl-theme media-center-carousel carousel-nav-middle" id="media-center-carousel">

            <?php for($i=0;$i<sizeof($eci_media_list);$i++) { ?>
            <?php 
            $eci_media_title = $eci_media_list[$i]['eci_media_title'];
            $eci_media_image_id = $eci_media_list[$i]['eci_media_image'];
            $eci_media_image_alt = get_post_meta( $eci_media_image_id, '_wp_attachment_image_alt', true );
            $eci_media_image_values = wp_get_attachment_image_src( $eci_media_image_id, 'slider-image' );
            $eci_media_image_url = $eci_media_image_values[0];
            $eci_media_link = $eci_media_list[$i]['eci_media_link_url'];
            $eci_media_link_url = $eci_media_link['url'];
            $eci_media_link_text = $eci_media_link['text'];
            $eci_media_link_target = $eci_media_link['target'];
            ?>

            <div class="item">
                <div class="media-item-thumb">
                    <div class="media-image">
                      <a href="<?php if( FALSE != $eci_media_link_url ) { echo $eci_media_link_url; } else { echo 'javascript:void();'; } ?>" <?php if( ( FALSE != $eci_media_link_target ) || ( $eci_media_link_target == 'none') ) { echo 'target="'.$eci_media_link_target.'"'; } ?> ><img src="<?php if( FALSE != $eci_media_image_url) { echo $eci_media_image_url; } else { echo get_template_directory_uri().'/images/media-center-image-default.jpg'; } ?>" alt="<?php if( FALSE != $eci_media_image_alt) { echo $eci_media_image_alt; } else { echo 'Media center image'; } ?>"></a>
                    </div>
                    <div class="media-body">
                      <div class="content">
                        <h3><a href="<?php if( FALSE != $eci_media_link_url ) { echo $eci_media_link_url; } else { echo 'javascript:void();'; } ?>" <?php if( ( FALSE != $eci_media_link_target ) || ( $eci_media_link_target == 'none') ) { echo 'target="'.$eci_media_link_target.'"'; } ?> ><?php echo $eci_media_title; ?></a></h3>
                        <?php if( FALSE != $eci_media_link ) { ?>
                          <p><a href="<?php if( FALSE != $eci_media_link_url ) { echo $eci_media_link_url; } else { echo 'javascript:void();'; } ?>" <?php if( ( FALSE != $eci_media_link_target ) || ( $eci_media_link_target == 'none') ) { echo 'target="'.$eci_media_link_target.'"'; } ?> ><?php if( FALSE != $eci_media_link_text ) { echo $eci_media_link_text; } else { echo 'View More'; } ?></a></p>
                        <?php } ?>
                      </div>
                    </div>
                </div>
            </div>

            <?php } ?>

            <?php /* ?><div class="item">
                <div class="media-item-thumb">
                    <div class="media-image">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/media-center-image.jpg" alt="Media Center Image">
                    </div>
                    <div class="media-body">
                      <div class="content">
                        <h3>Ali Hamdan Ahmed, Director of International and Regional Financial Organisations Department</h3>
                        <p><a href="#">View More</a></p>
                      </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="media-item-thumb">
                    <div class="media-image">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/media-center-image.jpg" alt="Media Center Image">
                    </div>
                    <div class="media-body">
                      <div class="content">
                        <h3>Ali Hamdan Ahmed, Director of International and Regional Financial Organisations Department</h3>
                        <p><a href="#">View More</a></p>
                      </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="media-item-thumb">
                    <div class="media-image">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/media-center-image.jpg" alt="Media Center Image">
                    </div>
                    <div class="media-body">
                      <div class="content">
                        <h3>Ali Hamdan Ahmed, Director of International and Regional Financial Organisations Department</h3>
                        <p><a href="#">View More</a></p>
                      </div>
                    </div>
                </div>
            </div><?php */ ?>
            
        </div>

        
      </div>
  </div>
</div>

<?php } else { ?>

<div class="media-center-block block-padding-top">
  <div class="container the-container media-center">
    <div class="heading-wrapper block-padding-bottom text-center">
        <h2><?php echo __('Media Center','eci'); ?></h2>
    </div>
    <div class="content text-center white-bg block-padding-top block-padding-bottom no-content-div">
        <p><?php echo __('No Content Found','eci'); ?></p>
    </div>
  </div>
</div>

<?php } ?>

<?php } ?>




<?php $show_contact_block = types_render_field( 'show-contact-block' ); ?>
<?php $show_contact_information_block = types_render_field( 'show-contact-information-block' ); ?>
<?php if( $show_contact_block == "1") { ?>

<div class="contact-block">
    <div id="contact-us"></div>
    <div id="map" class="map"></div>
    <div class="location-icon" id="map-location-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/images/location-icon.png" alt="Location Icon">
    </div>
    <?php if( $show_contact_information_block == "1") { ?>
    <div class="contact-info-block">
        <div class="contact-container">

            <div class="contact-details-wrap">
                <div class="contact-details">
                    <div class="heading-wrapper">
                        <h2><?php echo __('Contact Us','eci'); ?></h2>
                    </div>
                    <?php 
                    $contact_address = types_render_field( 'contact-address', array( 'output' => 'raw', 'separator' => '|' ) );
                    $contact_addresses = ( explode("|", $contact_address) );
                    ?>

                    <?php if( FALSE != $contact_address ) { ?>
                    <div class="address-block">
                        <div class="row address-row">

                            <?php foreach( $contact_addresses as $contact_addresse_item ) { ?>

                            <div class="col-xs-6 address-block-col">
                                <div class="content">
                                    <p><?php echo $contact_addresse_item; ?></p>
                                </div>
                            </div>

                            <?php } ?>

                            <?php /* ?><div class="col-xs-6 address-block-col">
                                <div class="content">
                                    <p>P.O BOX 94200<br/>
                                        Abu Dhabi, UAE</p>
                                </div>
                            </div>
                            <div class="col-xs-6 address-block-col">
                                <div class="content">
                                    <p>P.O BOX 29595<br/>
                                        Dubai, UAE</p>
                                </div>
                            </div><?php */ ?>

                        </div>
                    </div>
                    <?php } ?>

                    <?php 
                    $contact_telephone = types_render_field( 'contact-telephone', array( 'output' => 'raw', 'separator' => ', ' ) );
                    $contact_dir = types_render_field( 'contact-dir', array( 'output' => 'raw', 'separator' => ', ' ) );
                    $contact_email = types_render_field( 'contact-email', array( 'output' => 'raw', 'separator' => ', ' ) );
                    // $contact_telephones = ( explode("|", $contact_telephone) );
                    ?>

                    <div class="contact-media-block">
                        <p>Tel: <?php echo $contact_telephone; ?><br/>
                        Dir: <?php echo $contact_dir; ?><br/>
                        Email: <?php echo $contact_email; ?></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<?php } ?>

<?php endwhile; endif; wp_reset_postdata(); ?>

<?php
get_footer();